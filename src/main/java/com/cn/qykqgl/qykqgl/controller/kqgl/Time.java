package com.cn.qykqgl.qykqgl.controller.kqgl;

import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

@Configuration
@EnableScheduling
public class Time {
    @Scheduled(cron = "0 */1 * * * ?")
    public void time(){
        try {
            System.out.println("----------富二代开始吃海鲜----------");
            Thread.sleep(2000);
            System.out.println("----------富二代开始吃波士顿龙虾----------");
            Thread.sleep(2000);
            System.out.println("----------富二代喝了杯红酒----------");
            Thread.sleep(2000);
            System.out.println("----------富二代吃完了----------");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
