# 企业考勤管理系统

### 介绍
- 基于SpringBoot-Layui开发的学生版企业考勤管理系统完整代码; 可供练习和毕业设计学习参考.
- 分别为员工打卡系统和企业考勤管理两个系统
- 其中企业考勤管理系统主要功能包括: 员工信息管理、考勤信息管理、考勤统计、操作日志信息、登录日志信息、系统用户管理等功能

### 开发环境
- JDK 1.8
- Maven latest
- Spring Boot 2.4.5
- Intellij IDEA
- mysql 5.7
- git 版本管理
- layui 前端组件

### 使用说明
- 本地登陆地址:localhost:8099/web/jsp/login.jsp
- 拉取项目不要修改项目名称和注意存放路径,否则会发生报错情况
- 从Git上获取代码后，通过IDE导入此Maven工程
- 需要修改SpringBoot配置文件application.yml数据库配置,修改成你的数据库地址和访问用户
![.](https://images.gitee.com/uploads/images/2021/0517/155317_aade2297_7683706.png "屏幕截图.png")

### 演示

![登录界面](https://images.gitee.com/uploads/images/2021/0517/153649_f90e3436_7683706.png "屏幕截图.png")
登录界面

![首页](https://images.gitee.com/uploads/images/2021/0517/153724_d472b75e_7683706.png "屏幕截图.png")
首页

![员工信息管理界面](https://images.gitee.com/uploads/images/2021/0517/153905_8d137fcc_7683706.png "屏幕截图.png")
员工信息管理界面

![考勤信息管理界面](https://images.gitee.com/uploads/images/2021/0517/153940_535105cb_7683706.png "屏幕截图.png")
考勤信息管理界面

![操作日志信息界面](https://images.gitee.com/uploads/images/2021/0517/163423_2bc640ba_7683706.png "屏幕截图.png")
操作日志信息界面

![登录日志信息界面](https://images.gitee.com/uploads/images/2021/0517/163508_baddde22_7683706.png "屏幕截图.png")
登录日志信息界面

![系统用户界面](https://images.gitee.com/uploads/images/2021/0517/163650_8c417611_7683706.png "屏幕截图.png")
系统用户界面

![个人信息界面](https://images.gitee.com/uploads/images/2021/0517/154013_c113743f_7683706.png "屏幕截图.png")
个人信息界面

![修改密码](https://images.gitee.com/uploads/images/2021/0517/155913_74a5095a_7683706.png "屏幕截图.png")
修改密码

![新增界面](https://images.gitee.com/uploads/images/2021/0517/154039_ede3fdf1_7683706.png "屏幕截图.png")
新增界面

![打卡界面](https://images.gitee.com/uploads/images/2021/0517/155748_e100f8c9_7683706.png "屏幕截图.png")
打卡界面

### PS
- 本项目仅用于学习练习,严禁转卖!!!
- 在此欢迎大家交流，拍砖，反馈，共同进步，共同学习，共同成长。
- 不足的地方望大家多多包涵，后期会越做越好。
- 如有问题,请联系QQ:1196302555
- Email:1196302555@qq.com
- CSDN个人首页:https://blog.csdn.net/weixin_47390965?spm=1001.2101.3001.5343
- 如果帮助到您,请奖励小编一根冰淇淋吧!谢谢^_^

![输入图片说明](https://images.gitee.com/uploads/images/2021/0517/163202_0b802ec8_7683706.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0517/163210_6aabbdc6_7683706.png "屏幕截图.png")
